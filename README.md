This is my solution for the build of the unleash careers page. First a couple of house rules.

## start the project
- run `npm install` to install all npm packages
- run `npm start` to start the project. I'v used Create React App so it will start automatically
- For linting, run `npm run lint`. You should see no lintin errors. I like to use eslint to keep code consistent and find the AirBnB base settings the most opinionated but good to work with. Small modifications can be found in the `.eslintrc.json`
- I added a couple of basic test examples, which can be run with `npm run test`. It uses Jest out of the box. I have an example using enzyme for lifecycle testing, snapshot for smokebomb testing and one for testing my reducer/sagas.
- To watch and compile the sass files to css(which now is already included) you can run `npm run watch-css`
- To build, run `npm run build`, no changes from CRA here.

## Folder explanation
I like to keep my folders structured based on realms, so I have:
- components(individual components that can be used independently anywhere)
- data(any data used, for now I pass this on directly to the components through the page but normally this would be more API driven. I actually added a simple example for an API driven approach for the job listing)
- pages(pages that will be rendered by a router, like views)
- helpers(for now not much in there, this will be populated with cross app functions to use)
- services(the business logic, it includes the redux store, the actions/reducers and the sagas for side effects. It also would include a general API abstraction but for now I created static promise returns since I will deploy this to a static site host)
- assets(assets being used, like the images)
- icons(I normally convert icons to react components, so I can include them as a component and modify them in the context)

## Technologies
### Sass
I still enjoy writing css, but I keep my sass files(and compiled css files) next to the component. Preferable I would scope my classnames using css modules but for now I used a BEM like naming convention

### Redux Saga
I can't live without this. Generator style side effect handling with a great API and really easy to test. It creates a bit more boilerplate but I find on the long run it's a great way of working

### React Router
Not much to say about this, it's awesome. I tried to have a simple example showing routing based on the job openings.

### CSS grid
I used css grid since the browser support is actually quite good now. And as a fall back the implementation doesn't break the page, but it will be aesthetically a bit less pleasing.

## Honorable mentions
- I added the careers page to show the components used where properly self contained. There was no extra work added.
- There's a couple of differences between the designs and the current live site. The designs actually has some decisions I personally wouldn't implement, for example the image grid that on desktop suddenly has the content within it and the shifting border bottom on the images. Both of them would put some constraints on the build so I was happy to see the current site implementation doesn't have them.