import React from 'react'
import { string } from 'prop-types'

const CareerIcon = ({ width, fill }) => (
  <svg
    height={width}
    width={width}
    viewBox="0 0 51 51"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <use xlinkHref="#path0_fill" transform="translate(4.2 4.2)" fill={fill} />
    <defs>
      <path
        id="path0_fill"
        fillRule="evenodd"
        d="M21.25 21.25c-4.688 0-8.5-3.812-8.5-8.5 0-4.688 3.812-8.5 8.5-8.5 4.688 0 8.5 3.812 8.5 8.5 0 4.688-3.812 8.5-8.5 8.5zm7.986 1.43A12.715 12.715 0 0 0 34 12.75C34 5.708 28.292 0 21.25 0S8.5 5.708 8.5 12.75c0 4.016 1.861 7.595 4.764 9.93C5.49 25.602 0 32.82 0 42.5h4.25c0-10.625 7.627-17 17-17 9.373 0 17 6.375 17 17h4.25c0-9.68-5.489-16.898-13.264-19.82z"
      />
    </defs>
  </svg>
)

CareerIcon.propTypes = {
  width: string,
  fill: string,
}

CareerIcon.defaultProps = {
  width: '40px',
  fill: '#ECEBEC',
}

export default CareerIcon
