import React from 'react'
import cx from 'classnames'
import { arrayOf, shape, string, bool } from 'prop-types'
import './ImageGrid.css'

const ImageGrid = ({ images }) => (
  <div className="ImageGrid">
    { images.map(image => (
      <div
        className={cx('ImageGrid_image', { big: image.big })}
        key={image.alt}
      >
        <img
          src={image.src}
          alt={image.alt}
        />
      </div>
    ))}
  </div>
)

ImageGrid.propTypes = {
  images: arrayOf(shape({
    image: string,
    alt: string,
    big: bool,
  })).isRequired,
}

export default ImageGrid
