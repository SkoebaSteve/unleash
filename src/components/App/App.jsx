import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import './App.css'
import store from '../../services/store'
import CareersPage from '../../pages/CareersPage'
import PositionPage from '../../pages/PositionPage'
import ScrollToTop from '../../components/ScrollToTop'

const App = () => (
  <BrowserRouter>
    <Provider store={store}>
      <ScrollToTop>
        <div className="App">
          <Switch>
            <Redirect exact from="/" to="/careers" />
            <Route path="/careers" component={CareersPage} />
            <Route path="/position/:position" component={PositionPage} />
          </Switch>
          {/* <Careers /> */}
        </div>
      </ScrollToTop>
    </Provider>
  </BrowserRouter>
)

export default App
