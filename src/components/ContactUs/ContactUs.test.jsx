import React from 'react'
import renderer from 'react-test-renderer'
import ContactUs from './'
import data from '../../data'

const { pages: { careers: { contactUs } } } = data

it('renders correctly', () => {
  const tree = renderer
    .create(<ContactUs
      {...contactUs}
    />)
    .toJSON()
  expect(tree).toMatchSnapshot()
})
