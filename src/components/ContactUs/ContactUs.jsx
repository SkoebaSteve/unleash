import React from 'react'
import { string } from 'prop-types'
import './ContactUs.css'
import LinkedInIcon from '../../icons/LinkedInIcon'
import TwitterIcon from '../../icons/TwitterIcon'

const ContactUs = ({ heading, image, name, title, email, linkedIn, twitter }) => (
  <div className="ContactUs">
    <div className="container">
      <h2 className="ContactUs_Heading">{heading}</h2>
      <div className="ContactUs-Wrapper">
        <div className="ContactPerson">
          <div className="ContactPerson_Avatar">
            <img src={image} alt="our contact person" />
          </div>
          <div className="ContactPerson_Content">
            <h3 className="ContactPerson_Name">{name}</h3>
            <p className="ContactPerson_Title">{title}</p>
          </div>
        </div>
        <div className="ContactDetails">
          <a className="ContactDetails_Email" href={`mailto:${email}`}>{email}</a>
          <div className="ContactDetails_Social">
            { linkedIn &&
              <a className="ContactDetails_LinkedIn" href={linkedIn}>
                <LinkedInIcon />
              </a>
            }
            { twitter &&
              <a className="ContactDetails_LinkedIn" href={twitter}>
                <TwitterIcon />
              </a>
            }
          </div>
        </div>
      </div>
    </div>
  </div>
)

ContactUs.propTypes = {
  /* eslint-disable react/no-typos */
  heading: string.isRequired,
  image: string.isRequired,
  name: string.isRequired,
  title: string.isRequired,
  email: string.isRequired,
  /* eslint-enable react/no-typos */
  linkedIn: string,
  twitter: string,
}

ContactUs.defaultProps = {
  linkedIn: undefined,
  twitter: undefined,
}

export default ContactUs
