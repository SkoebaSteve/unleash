import React from 'react'
import { string } from 'prop-types'
import './VideoWrapper.css'

const VideoWrapper = ({ title, url }) => (
  <div className="VideoWrapper">
    <iframe
      className="VideoWrapper_iframe"
      title={title}
      src={url}
      frameBorder="0"
    />
  </div>
)

VideoWrapper.propTypes = {
  /* eslint-disable react/no-typos */
  title: string.isRequired,
  url: string.isRequired,
  /* eslint-enable react/no-typos */
}

export default VideoWrapper
