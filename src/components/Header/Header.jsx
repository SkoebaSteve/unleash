import React from 'react'
import { string } from 'prop-types'
import './Header.css'

const Header = ({ title }) => (
  <header className="header">
    <div className="container">
      <h1 className="header_title">{ title }</h1>
    </div>
  </header>
)

/* eslint-disable react/no-typos */
Header.propTypes = { title: string.isRequired }
/* eslint-enable react/no-typos */

export default Header

