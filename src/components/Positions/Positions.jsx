import React from 'react'
import { connect } from 'react-redux'
import { string, arrayOf, func, shape } from 'prop-types'
import Position from './Position'
import './Positions.css'
import { positionsFetchRequested } from '../../services/actions'

class Positions extends React.Component {
  componentDidMount() {
    // mount dependencies independently
    this.props.positionsFetchRequested()
  }
  render() {
    const { positions } = this.props
    return (
      <div className="Positions">
        <h2 className="Positions_Title">Open positions</h2>
        <ul className="Positions_List">
          { positions.map(position => (
            <li className="Positions_ListItem" key={position.title}>
              <Position
                avatar={position.avatar}
                title={position.title}
                employment={position.employment}
                location={position.location}
                link={position.link}
              />
            </li>
          ))
          }
        </ul>
      </div>
    )
  }
}

Positions.propTypes = {
  /* eslint-disable react/no-typos */
  positionsFetchRequested: func.isRequired,
  /* eslint-enable react/no-typos */
  // deep check object passed to be of the right format
  positions: arrayOf(shape({
    avatar: string,
    title: string,
    employment: string,
    location: string,
  })).isRequired,
}

const mapStateToProps = state => ({ positions: state.positions })
const mapDispatchToProps = { positionsFetchRequested }
export default connect(mapStateToProps, mapDispatchToProps)(Positions)
