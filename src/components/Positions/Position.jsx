import React from 'react'
import { Link } from 'react-router-dom'
import { string } from 'prop-types'
import CareerIcon from '../../icons/CareerIcon'
import './Positions.css'

const Position = ({ title, employment, location, link }) => (
  <Link className="Position" to={`/position/${link}`}>
    <div className="Position_Avatar">
      <CareerIcon />
    </div>
    <div className="Position_Content">
      <h3 className="Position_Title">{title}</h3>
      <p className="Position_Employment">{employment}</p>
      <p className="Position_Location">{location}</p>
    </div>
  </Link>
)

Position.propTypes = {
  /* eslint-disable react/no-typos */
  title: string.isRequired,
  employment: string.isRequired,
  location: string.isRequired,
  link: string.isRequired,
  /* eslint-enable react/no-typos */
}

export default Position
