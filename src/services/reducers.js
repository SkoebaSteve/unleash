import { combineReducers } from 'redux'
import positions from './positions/positionReducers'

const reducers = combineReducers({
  positions,
})

export default reducers
