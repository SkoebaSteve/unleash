import { fork } from 'redux-saga/effects'
import positionsSaga from './positions/positionsSagas'

function* sagas() {
  yield [
    fork(positionsSaga),
  ]
}

export default sagas
