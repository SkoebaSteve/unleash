import data from '../../data'

// destructure only the blocks needed from data
const { positions } = data
const fetchPositions = () => (
  new Promise((resolve, reject) => {
    resolve(positions)
    reject(new Error('something went wrong'))
  })
)

export default fetchPositions
