const positions = (state = [], action) => {
  switch (action.type) {
    case 'POSITIONS_FETCH_SUCCEEDED':
      return action.positions
    case 'POSITIONS_FETCH_FAILED':
      return state
    default:
      return state
  }
}

export default positions
