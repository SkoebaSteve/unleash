export const positionsFetchRequested = () => ({
  type: 'POSITIONS_FETCH_REQUESTED'
})

export const positionsFetchSucceeded = positions => ({
  type: 'POSITIONS_FETCH_SUCCEEDED',
  positions,
})

export const positionsFetchFailed = error => ({
  type: 'POSITIONS_FETCH_FAILED',
  error,
})
