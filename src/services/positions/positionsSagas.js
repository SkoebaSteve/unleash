import { call, put, takeLatest } from 'redux-saga/effects'
import fetchPositions from './positionsApi'
import {
  positionsFetchSucceeded,
  positionsFetchFailed,
} from './positionsActions'

// worker Saga: will be fired on MESSAGES_FETCH_REQUESTED actions
export function* getPositions() {
  try {
    const positions = yield call(fetchPositions)
    yield put(positionsFetchSucceeded(positions))
  } catch (e) {
    yield put(positionsFetchFailed(e.message))
  }
}

export default function* positionsSaga() {
  yield takeLatest('POSITIONS_FETCH_REQUESTED', getPositions)
}
