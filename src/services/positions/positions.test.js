/* eslint-env jest */
import { call, put } from 'redux-saga/effects'
import positions from './positionReducers'
import {
  positionsFetchSucceeded,
  positionsFetchFailed,
} from './positionsActions'

import { getPositions } from './positionsSagas'
import fetchPositions from './positionsApi'

const testPositions = [
  {
    title: 'Senior Frontend Developer',
    employment: 'Full time job',
    location: 'Budapest',
  },
  {
    title: 'Senior Backend Developer',
    employment: 'Full time job',
    location: 'Budapest',
  },
]

/* TESTING REDUCERS */

describe('positions reducers', () => {
  it('should return an empty state', () => {
    expect(positions(undefined, {})).toEqual([])
  })

  it('should return positions on POSITIONS_FETCH_SUCCEEDED', () => {
    expect(positions([], positionsFetchSucceeded(testPositions))).toEqual(testPositions)
  })

  it('should return positions on POSITIONS_FETCH_FAILED', () => {
    expect(positions(testPositions, positionsFetchFailed('error'))).toEqual(testPositions)
  })
})

/* TESTING SAGAS */

describe('message saga', () => {
  // create new generators
  const positionsGen = getPositions()
  const positionsErrorGen = getPositions()
  it('should fire the positions fetch function', () => {
    // next indicates a step, so it should be equal to the 'yield' step in the saga
    expect(positionsGen.next().value)
      .toEqual(call(fetchPositions))
  })
  it('should dispatch the fetched positions success action', () => {
    // second step should be the yield of the success action if not thrown
    expect(positionsGen.next(testPositions).value)
      .toEqual(put(positionsFetchSucceeded(testPositions)))
  })
  it('should dispatch the fetched positions error action', () => {
    // trigger first call step
    positionsErrorGen.next()
    // second step should be the yield of the error action if thrown
    expect(positionsErrorGen.throw(new Error('error')).value)
      .toEqual(put(positionsFetchFailed('error')))
  })
})
