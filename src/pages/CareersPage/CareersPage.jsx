import React from 'react'
import Header from '../../components/Header'
import ImageGrid from '../../components/ImageGrid'
import VideoWrapper from '../../components/VideoWrapper'
import Positions from '../../components/Positions'
import ContactUs from '../../components/ContactUs'
import data from '../../data'
import './CareersPage.css'

// destructure only the blocks needed from data
const { pages: { careers: { contentBlock, video } }, contactUs } = data

const CareersPage = () => (
  <article className="page">
    <Header title="Careers" />
    <div className="container">
      <section className="row careersBlock">
        <div className="imagesBlock">
          <ImageGrid images={contentBlock.images} />
        </div>
        <div className="contentBlock">
          <h2 className="contentBlock_header">{ contentBlock.title }</h2>
          <p className="contentBlock_description">{ contentBlock.description }</p>
        </div>
      </section>
      <section className="row">
        <VideoWrapper
          url={video.url}
          title={video.title}
        />
      </section>
      <section className="row">
        <Positions />
      </section>
    </div>
    <section>
      <ContactUs
        heading={contactUs.heading}
        image={contactUs.image}
        name={contactUs.name}
        title={contactUs.title}
        email={contactUs.email}
        linkedIn={contactUs.linkedIn}
      />
    </section>
  </article>
)

export default CareersPage
