import React from 'react'
import { match as matchProps } from 'react-router-prop-types'
import Header from '../../components/Header'
import Positions from '../../components/Positions'
import ContactUs from '../../components/ContactUs'
import data from '../../data'
import './PositionsPage.css'

const {
  positions,
  companyInfo,
  contactUs,
} = data

const PositionsPage = ({ match }) => {
  const position = positions.filter(positionItem => (
    positionItem.link === match.params.position
  ))[0]
  return (
    <article className="PositionsPage">
      <Header title={position.title} />
      <div className="container">
        <div className="PositionsPage_About">
          <span className="PositionsPage_Badge">{position.location}</span>
          <span className="PositionsPage_Badge">{position.employment}</span>
        </div>
        <section className="PositionsPage_Content">
          <h2 className="PositionsPage_Header">{companyInfo.title}</h2>
          <p>{companyInfo.description}</p>
        </section>
        <section className="PositionsPage_Content">
          <h2 className="PositionsPage_Header">Job Description</h2>
          <p>{position.description}</p>
        </section>
        <section className="row">
          <Positions />
        </section>
      </div>
      <section>
        <ContactUs
          heading={contactUs.heading}
          image={contactUs.image}
          name={contactUs.name}
          title={contactUs.title}
          email={contactUs.email}
          linkedIn={contactUs.linkedIn}
        />
      </section>
    </article>
  )
}

PositionsPage.propTypes = { match: matchProps.isRequired }

export default PositionsPage

