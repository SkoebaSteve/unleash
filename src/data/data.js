
const data = {
  pages: {
    careers: {
      contentBlock: {
        title: 'GLOBAL OPPORTUNITIES',
        description: 'We are passionate creative people on a mission to inspire and transform the world of work & technology.Our shared vision is that by 2020, UNLEASH will be the platform of choice globally on the Future of Work. With more than 21 number of nationalities represented at our company, we pride ourselves on truly being an international business.',
        images: [
          {
            // reference the file path through require for now, this could be a path from an upload directory
            src: require('../assets/images/careers-big.jpg'),
            alt: 'image of Budapest castle',
            big: true,
          },
          {
            src: require('../assets/images/careers-smallOne.jpg'),
            alt: 'image of ferris wheel',
          },
          {
            src: require('../assets/images/careers-smallTwo.jpg'),
            alt: 'image of skyscrapers',
          }
        ]
      },
      video: {
        url: 'https://www.youtube.com/embed/2FcXP9dPTIk?rel=0&amp;hd=1',
        title: 'A Golden Hour by the Danube River in Budapest 4K',
      },
    },
  },
  positions: [
    {
      title: 'Senior Frontend Developer',
      employment: 'Full time job',
      location: 'Budapest',
      link: 'senior-frontend-developer',
      description: "As a Senior Frontend Developer you’ll be serving as an expert and leader in the development projects of UNLEASH's online presence, making sure it looks amazing and get’s those Oooohs and Aaaahs from our users. You'll take over the building of our event websites in cooperation with a world class UX Design and Developer team. This position plays a key role in leading the creation, delivery and maintenance of digital experiences of greatest events about the Future of Work. You`ll be demonstrating your technical aptitude and ability to embrace software development best practices in support of department goals.",
    },
    {
      title: 'Senior Backend Developer',
      employment: 'Full time job',
      location: 'Budapest',
      link: 'senior-backend-developer',
    },
    {
      title: 'Content Manager',
      employment: 'Full time job',
      location: 'Budapest',
      link: 'content-manager',
    },
    {
      title: 'International Delegate Sales Executive',
      employment: 'Full time job',
      location: 'Budapest',
      link: 'international-delegate-sales-executive',
    },
    {
      title: 'Conference Sales Manager',
      employment: 'Full time job',
      location: 'Budapest',
      link: 'conference-sales-manager',
    },
  ],
  companyInfo: {
    title: 'Company Description',
    description: 'We are passionate creative people on a mission to inspire and transform the world of work & technology. Our shared vision is that by 2020 UNLEASH will be the platform of choice on the Future of Work across the globe. Shows that attract the world’s leading entrepreneurs, visionaries, disrupters and doers including Sir Richard Branson, Arianna Huffington, Sir Ken Robinson, Rachel Botsman, Gary Vaynerchuk and many more. Over 55% of our community are CEO’s, CPOs, EVPs, and SVPs of the most exciting brands and leading organizations from +120 countries worldwide. UNLEASH (previously HR Tech World) is much more than just business events; we are in the change the world for the greater good business.',
  },
  contactUs: {
    heading: 'contact us if you have more questions',
    image: require('../assets/images/contactPerson-Elena.png'),
    name: 'Elena Markina',
    title: 'Chief People Officer',
    email: 'elena@unleashgroup.io',
    linkedIn: 'https://www.linkedin.com/in/elenamarkina/',
  },
}

export default data
